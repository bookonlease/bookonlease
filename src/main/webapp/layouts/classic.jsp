<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
  <head>
     <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
     <link href="css/custom.css" rel="stylesheet">
      
  </head>
  <body style="width: outo; height: 500px;">
        <table border="1" cellspacing="0" cellpadding="0" style="width: 100%;height: 130%" >
        <tr align="center" bgcolor="">
        <td colspan="2" style="width: 100px;height: 40px;" ><tiles:insertAttribute name="header"></tiles:insertAttribute></td>
        </tr>
        
        <tr align="center">
		<%-- <td style="width: 20%" bgcolor=""><tiles:insertAttribute name="menu"></tiles:insertAttribute></td>
 --%>		<td style="width: 80%"><tiles:insertAttribute name="body"></tiles:insertAttribute></td>
		</tr>
		
		<tr align="center">
		 <td colspan="2" style="width: 100px;height: 50px;" bgcolor=""><tiles:insertAttribute name="footer"></tiles:insertAttribute></td> 
    
    
    
    </table>
  </body>
</html>